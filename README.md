# README #

Backend for Fintify based on Django

### What is this repository for? ###

* The pipline_scrapper backend

### How do I get set up? ###

* Clone code (git clone <repo>)

* Create and activate virtualenviroment using python3 
	- virtualenv -p python3 venv
	- source venv/bin/activate

* Install all libraries from "requirements.txt" file ("pip install -r requirements.txt")

* Create a postgres database and update the settings in application (pipline_scraper/setting.py)

* After that you can go to the top project folder (where "manage.py" is located) and run:
	- python manage.py migrate
	- python manage.py runserver 127.0.0.1:8000

* To scheude the cron to run and scrap data on daily basis run: 
	- python manage.py installtasks. 
	- It will set cronjobs to run daily at 12:00 am.

* To start scraping manually run management command.
	- python manage.py anr_data_selenium
	- python manage.py pepl_data_selenium
	- python manage.py tigt_data_selenium
	- python manage.py tpc_data_selenium
	- python manage.py alliance_data_selenium