"""OS Modules environ method to get the setup vars from the Environment"""
from datetime import datetime
from os import environ

from random import randint
from random import sample
from math import ceil
from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.chrome.options import Options
from selenium.webdriver import DesiredCapabilities
from selenium.webdriver.firefox.webdriver import FirefoxProfile

import random


class SeleniumPy:
    """Class to be instantiated to use the script"""

    def __init__(self, nogui=False, selenium_local_session=True, use_firefox=False, page_delay=25):
        if nogui:
            self.display = Display(visible=0, size=(800, 600))
            self.display.start()

        self.browser = None

        # self.logFile = open('./logs/logFile.txt', 'w')

        self.nogui = nogui

        self.page_delay = page_delay
        self.switch_language = True
        self.use_firefox = use_firefox
        self.firefox_profile_path = None

        if selenium_local_session:
            self.set_selenium_local_session()

    def set_selenium_local_session(self):
        """Starts local session for a selenium server. Default case scenario."""
        if self.use_firefox:
            if self.firefox_profile_path is not None:
                firefox_profile = webdriver.FirefoxProfile(self.firefox_profile_path)
            else:
                firefox_profile = webdriver.FirefoxProfile()

            # permissions.default.image = 2: Disable images load, this setting can improve pageload & save bandwidth
            firefox_profile.set_preference('permissions.default.image', 2)

            self.browser = webdriver.Firefox(firefox_profile=firefox_profile)

        else:
            chromedriver_location = './chromedriver'
            chrome_options = Options()
            chrome_options.add_argument('--dns-prefetch-disable')
            chrome_options.add_argument('--no-sandbox')
            chrome_options.add_argument('--lang=en-US')

            # managed_default_content_settings.images = 2: Disable images load, this setting can improve pageload & save bandwidth
            # default_content_setting_values.notifications = 2: Disable notifications
            # credentials_enable_service & password_manager_enabled = false: Ignore save password prompt from chrome
            # 'profile.managed_default_content_settings.images': 2,
            # 'profile.default_content_setting_values.notifications' : 2,
            # 'credentials_enable_service': False,
            # 'profile': {
            #   'password_manager_enabled': False
            # }

            chrome_prefs = {
                'intl.accept_languages': 'en-US'
            }
            chrome_options.add_experimental_option('prefs', chrome_prefs)
            self.browser = webdriver.Chrome(chromedriver_location, chrome_options=chrome_options)
        self.browser.implicitly_wait(self.page_delay)
        # self.logFile.write('Session started - %s\n' \
        #                    % (datetime.now().strftime('%Y-%m-%d %H:%M:%S')))

        return self

    def set_selenium_remote_session(self, selenium_url=''):
        """Starts remote session for a selenium server. Useful for docker setup."""
        if self.aborting:
            return self

        self.browser = webdriver.Remote(command_executor=selenium_url,
                                        desired_capabilities=DesiredCapabilities.CHROME)
        self.browser.maximize_window()
        # self.logFile.write('Session started - %s\n' \
        #                    % (datetime.now().strftime('%Y-%m-%d %H:%M:%S')))

        return self

    def end(self):
        """Closes the current session"""
        dump_follow_restriction(self.follow_restrict)
        self.browser.delete_all_cookies()
        self.browser.close()

        if self.nogui:
            self.display.stop()

        print('')
        print('Session ended')
        print('-------------')

        # self.logFile.write(
        #     '\nSession ended - {}\n'.format(
        #         datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        #     )
        # )
        # self.logFile.write('-' * 20 + '\n\n')
        # self.logFile.close()

        with open('./logs/followed.txt', 'w') as followFile:
            followFile.write(str(self.followed))
