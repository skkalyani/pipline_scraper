from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(ANR)
admin.site.register(PEPL)
admin.site.register(TIGT)
admin.site.register(TPC)
admin.site.register(Alliance)
