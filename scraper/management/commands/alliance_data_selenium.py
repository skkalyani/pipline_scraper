from django.core.management.base import BaseCommand, CommandError
import requests
from bs4 import BeautifulSoup
from pipline_scraper.selenium_py import SeleniumPy
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import datetime
from scraper.models import Alliance
import kronos

@kronos.register('0 0 * * *')
class Command(BaseCommand):
    def handle(self, *args, **options):
        session = SeleniumPy(nogui=True)
        start_date = datetime.datetime.now().date()
        if Alliance.objects.all():
            end_date = Alliance.objects.all().order_by('-date')[0].date.date()
        else:
            end_date = start_date + datetime.timedelta(days=-3000)

        while (start_date>end_date):
            browser = session.browser
            browser.get('http://ips.alliance-pipeline.com/Ips/MainPage.aspx?siteId=1')
            browser.find_element_by_id("treeviewn0").click()
            browser.find_element_by_id("treeviewt1").click()
            try:
                myElem = WebDriverWait(browser, 60).until(EC.presence_of_element_located((By.CLASS_NAME, 'igg_Alt')))
                print ("Page is ready!")
            except TimeoutException:
                print ("Loading took too much time!")
            input_date = browser.find_elements_by_xpath("//input[@class='igte_EditInContainer']")
            input_date[0].clear()
            input_date[0].send_keys(start_date.strftime('%m/%d/%Y'))
            start_date = start_date + datetime.timedelta(days=-1)
            element = browser.find_elements_by_class_name("igg_Alt")
            # browser.execute_script("""var element = arguments[0];element.parentNode.removeChild(element);""", element)
            browser.execute_script("""
                var element = document.querySelector(".igg_FixedColumnCellCssClass");
                if (element)
                    element.parentNode.removeChild(element);
                """)
            browser.find_element_by_id("MainContent_btnRetrieveJobSchedForGrid").click()
            try:
                myElem = WebDriverWait(browser, 60).until(EC.presence_of_element_located((By.CLASS_NAME, 'igg_Alt')))
                print ("Page is ready!")
            except TimeoutException:
                print ("Loading took too much time!")
            html = browser.page_source
            soup = BeautifulSoup(html, 'html.parser')
            tbody = soup.find(class_="igg_FixedColumnCellCssClass")
            rows = tbody.findChildren('tr')
            for i in range(0, len(rows), 2):
                elements = rows[i].findChildren('td')
                all =  Alliance()
                all.location_name = elements[0].text.strip()
                all.location = elements[8].text.strip()
                all.loc_purp_desc = elements[7].text.strip()
                all.loc_qti = elements[9].text.strip()
                all.designcapacity = elements[11].text.strip()
                all.operatingcapacity = elements[12].text.strip()
                all.totalscheduledquantity = elements[13].text.strip()
                all.operationallyavailablecapacity = elements[14].text.strip()
                all.cycle = elements[4].text.strip()
                all.eff_gas_dat_time = datetime.datetime.strptime(elements[3].text.strip()+" "+elements[5].text.strip(), '%m/%d/%Y %H:%M')
                all.post_date_time = datetime.datetime.strptime(elements[2].text.strip(), '%m/%d/%Y %H:%M %p')
                all.flow_ind = elements[16].text.strip()
                all.it = elements[17].text.strip()
                all.qty_reason = elements[18].text.strip()
                all.all_qty_avail = elements[10].text.strip()
                all.save()