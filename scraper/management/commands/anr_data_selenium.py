from django.core.management.base import BaseCommand, CommandError
import requests
from bs4 import BeautifulSoup
from scraper.models import ANR
import datetime
from pipline_scraper.selenium_py import SeleniumPy
import time
import kronos

@kronos.register('0 0 * * *')
class Command(BaseCommand):
    def handle(self, *args, **options):
        session = SeleniumPy(nogui=True)
        start_date = datetime.datetime.now().date()
        if ANR.objects.all():
            end_date = ANR.objects.all().order_by('-date')[0].date.date()
        else:
            end_date = start_date + datetime.timedelta(days=-3000)

        while (start_date>end_date):
            browser = session.browser
            browser.get('http://anrebb.transcanada.com/AvailCap/HistAvailCapQuery.asp?sPipelineCode=ANR')
            input_date = browser.find_elements_by_xpath("//input[@name='frmStartDate']")
            input_date[0].clear()
            input_date[0].send_keys(start_date.strftime('%m/%d/%Y'))
            start_date = start_date + datetime.timedelta(days=-1)
            submit = browser.find_elements_by_xpath("//input[@type='submit']")
            submit[0].click()
            page = browser.page_source
            soup = BeautifulSoup(page, 'html.parser')
            data_table = soup.find('table', border="1")
            rows = data_table.findChildren('tr')
            print ("------------ No of rows ---------------")
            print(len(rows))
            for row in rows[1:-1]:
                elements = row.findChildren('small')
                anr =  ANR()
                anr.location_name = elements[0].text.strip()
                anr.location = elements[1].text.strip()
                anr.loc_purp_desc = elements[2].text.strip()
                anr.loc_qti = elements[3].text.strip()
                anr.dc = elements[4].text.strip()
                anr.opc = elements[5].text.strip()
                anr.tsq = elements[6].text.strip()
                anr.oac = elements[7].text.strip()
                anr.cycle = elements[8].text.strip()
                anr.eff_gas_dat_time = datetime.datetime.strptime(elements[9].text.strip(), '%m/%d/%Y %H:%M')
                anr.post_date_time = datetime.datetime.strptime(elements[10].text.strip(), '%m/%d/%Y %H:%M')
                anr.flow_ind = elements[11].text.strip()
                anr.it = elements[12].text.strip()
                anr.comments = elements[13].text.strip()
                anr.all_qty_avail = elements[14].text.strip()
                anr.save()