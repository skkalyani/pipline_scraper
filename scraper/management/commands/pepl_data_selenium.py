from django.core.management.base import BaseCommand, CommandError
import requests
from bs4 import BeautifulSoup
from pipline_scraper.selenium_py import SeleniumPy
from selenium.webdriver.common.by import By
import datetime
from scraper.models import PEPL
import kronos

@kronos.register('0 0 * * *')
class Command(BaseCommand):
    def handle(self, *args, **options):
        session = SeleniumPy(nogui=True)
        start_date = datetime.datetime.now().date()
        if PEPL.objects.all():
            end_date = PEPL.objects.all().order_by('-date')[0].date.date()
        else:
            end_date = start_date + datetime.timedelta(days=-3000)
        browser = session.browser
        browser.get('http://peplmessenger.energytransfer.com/ipost/PEPL/capacity/operationally-available-by-location')

        while (start_date>end_date):
            input_date = browser.find_elements_by_xpath("//input[@id='gasDay']")
            input_date[0].clear()
            input_date[0].send_keys(start_date.strftime('%m/%d/%Y'))
            start_date = start_date + datetime.timedelta(days=-1)
            submit = browser.find_elements_by_xpath("//button[@type='submit']")
            submit[0].click()
            html = browser.page_source
            soup = BeautifulSoup(html, 'html.parser')
            dates = soup.find_all(class_='pad')
            post_date_time = dates[0].text.split('\t')[1]
            eff_gas_dat_time = dates[1].text.split(': ')[1]
            data_table = soup.find('table')
            tbody = data_table.find('tbody')
            rows = tbody.findChildren("tr")
            for row in rows:
                elements = row.findChildren('td')
                pepl =  PEPL()
                pepl.location_name = elements[0].text.strip()
                pepl.location = elements[1].text.strip()
                pepl.loc_purp_desc = elements[2].text.strip()
                pepl.loc_qti = elements[3].text.strip()
                pepl.dc = elements[4].text.strip()
                pepl.opc = elements[5].text.strip()
                pepl.tsq = elements[6].text.strip()
                pepl.oac = elements[7].text.strip()
                pepl.loc_zn = elements[8].text.strip()
                pepl.it = elements[9].text.strip()
                pepl.flow_ind = elements[10].text.strip()
                pepl.state = elements[11].text.strip()
                pepl.country = elements[12].text.strip()
                pepl.operator = elements[13].text.strip()
                pepl.oba = elements[14].text.strip()
                pepl.gt = elements[15].text.strip()
                pepl.miles = elements[16].text.strip()
                pepl.egm = elements[17].text.strip()
                pepl.flw_cnt = elements[18].text.strip()
                pepl.all_qty_avail = elements[19].text.strip()
                pepl.qty_reason = elements[20].text.strip()

                pepl.eff_gas_dat_time = datetime.datetime.strptime(eff_gas_dat_time.strip(), '%m/%d/%Y %I:%M%p')
                pepl.post_date_time = datetime.datetime.strptime(post_date_time.strip(), '%m/%d/%Y %I:%M%p')
                pepl.save()