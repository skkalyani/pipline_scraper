from django.core.management.base import BaseCommand, CommandError
import requests
from bs4 import BeautifulSoup
from pipline_scraper.selenium_py import SeleniumPy
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import datetime
from scraper.models import TPC
import kronos

@kronos.register('0 0 * * *')
class Command(BaseCommand):
    def handle(self, *args, **options):
        session = SeleniumPy(nogui=True)
        start_date = datetime.datetime.now().date()
        if TPC.objects.all():
            end_date = TPC.objects.all().order_by('-date')[0].date.date()
        else:
            end_date = start_date + datetime.timedelta(days=-3000)

        while (start_date>end_date):
            browser = session.browser
            browser.get('http://pipeline.tallgrassenergylp.com/Pages/Point.aspx?pipeline=403&type=OA')
            input_date = browser.find_elements_by_xpath("//input[@id='mainContent_tbGasFlow']")
            input_date[0].clear()
            input_date[0].send_keys(start_date.strftime('%m/%d/%Y'))
            browser.find_element_by_id("mainContent_btnRetrieve").click()
            try:
                myElem = WebDriverWait(browser, 60).until(EC.presence_of_element_located((By.ID, 'mainContent_GridView1')))
                print ("Page is ready!")
            except TimeoutException:
                print ("Loading took too much time!")
            html = browser.page_source
            soup = BeautifulSoup(html, 'html.parser')
            tbody = soup.find(class_="GridStyle")
            rows = tbody.findChildren('tr')
            for row in rows[1:]:
                elements = row.findChildren('td')
                tpc =  TPC()
                tpc.location = elements[1].text.strip()
                tpc.location_name = elements[2].text.strip()
                tpc.bi_di = elements[3].text.strip()
                tpc.locsegment = elements[4].text.strip()
                tpc.all_qty_avail = elements[5].text.strip()
                tpc.designcapacity = elements[6].text.strip()
                tpc.unsubscribedcapacity = elements[7].text.strip()
                tpc.operatingcapacity = elements[8].text.strip()
                tpc.eff_gas_dat_time = start_date
                # tpc.post_date_time = datetime.datetime.strptime(elements[10].text.strip(), '%m/%d/%Y %H:%M')
                tpc.totalscheduledquantity = elements[9].text.strip()
                tpc.operationallyavailablecapacity = elements[10].text.strip()
                tpc.it = elements[11].text.strip()
                tpc.qty_reason = elements[12].text.strip()
                tpc.comments = elements[13].text.strip()
                tpc.save()
            start_date = start_date + datetime.timedelta(days=-1)