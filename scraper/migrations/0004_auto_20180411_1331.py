# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-04-11 13:31
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('scraper', '0003_anr_date'),
    ]

    operations = [
        migrations.CreateModel(
            name='PEPL',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('location_name', models.CharField(blank=True, max_length=255, null=True)),
                ('location', models.CharField(blank=True, max_length=255, null=True)),
                ('loc_purp_desc', models.CharField(blank=True, max_length=255, null=True)),
                ('loc_qti', models.CharField(blank=True, max_length=255, null=True)),
                ('loc_zn', models.CharField(blank=True, max_length=255, null=True)),
                ('dc', models.CharField(blank=True, max_length=255, null=True)),
                ('opc', models.CharField(blank=True, max_length=255, null=True)),
                ('tsq', models.CharField(blank=True, max_length=255, null=True)),
                ('oac', models.CharField(blank=True, max_length=255, null=True)),
                ('eff_gas_dat_time', models.DateTimeField(blank=True, null=True)),
                ('post_date_time', models.DateTimeField(blank=True, null=True)),
                ('flow_ind', models.CharField(blank=True, max_length=255, null=True)),
                ('it', models.CharField(blank=True, max_length=255, null=True)),
                ('comments', models.CharField(blank=True, max_length=255, null=True)),
                ('all_qty_avail', models.CharField(blank=True, max_length=255, null=True)),
                ('state', models.CharField(blank=True, max_length=255, null=True)),
                ('country', models.CharField(blank=True, max_length=255, null=True)),
                ('operator', models.CharField(blank=True, max_length=255, null=True)),
                ('oba', models.CharField(blank=True, max_length=255, null=True)),
                ('gt', models.CharField(blank=True, max_length=255, null=True)),
                ('miles', models.CharField(blank=True, max_length=255, null=True)),
                ('qty_reasion', models.CharField(blank=True, max_length=255, null=True)),
                ('flw_cnt', models.CharField(blank=True, max_length=255, null=True)),
                ('date', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'db_table': 'pepl',
                'verbose_name_plural': 'PEPL',
            },
        ),
        migrations.RenameField(
            model_name='anr',
            old_name='plow_ind',
            new_name='flow_ind',
        ),
    ]
