from django.db import models

# Create your models here.
class ANR(models.Model):
	location_name = models.CharField(max_length=255, blank=True, null=True)
	location = models.CharField(max_length=255, blank=True, null=True)
	loc_purp_desc = models.CharField(max_length=255, blank=True, null=True)
	loc_qti = models.CharField(max_length=255, blank=True, null=True)
	dc = models.CharField(max_length=255, blank=True, null=True)
	opc = models.CharField(max_length=255, blank=True, null=True)
	tsq = models.CharField(max_length=255, blank=True, null=True)
	oac = models.CharField(max_length=255, blank=True, null=True)
	cycle = models.CharField(max_length=255, blank=True, null=True)
	eff_gas_dat_time = models.DateTimeField(null=True, blank=True)
	post_date_time = models.DateTimeField(null=True, blank=True)
	flow_ind = models.CharField(max_length=255, blank=True, null=True)
	it = models.CharField(max_length=255, blank=True, null=True)
	comments = models.CharField(max_length=255, blank=True, null=True)
	all_qty_avail = models.CharField(max_length=255, blank=True, null=True)
	date = models.DateTimeField(auto_now_add=True)

	class Meta:
		db_table = 'anr'
		verbose_name_plural = "ANR"

class PEPL(models.Model):
	location_name = models.CharField(max_length=255, blank=True, null=True)
	location = models.CharField(max_length=255, blank=True, null=True)
	loc_purp_desc = models.CharField(max_length=255, blank=True, null=True)
	loc_qti = models.CharField(max_length=255, blank=True, null=True)
	loc_zn = models.CharField(max_length=255, blank=True, null=True)
	dc = models.CharField(max_length=255, blank=True, null=True)
	opc = models.CharField(max_length=255, blank=True, null=True)
	tsq = models.CharField(max_length=255, blank=True, null=True)
	oac = models.CharField(max_length=255, blank=True, null=True)
	eff_gas_dat_time = models.DateTimeField(null=True, blank=True)
	post_date_time = models.DateTimeField(null=True, blank=True)
	flow_ind = models.CharField(max_length=255, blank=True, null=True)
	it = models.CharField(max_length=255, blank=True, null=True)
	comments = models.CharField(max_length=255, blank=True, null=True)
	all_qty_avail = models.CharField(max_length=255, blank=True, null=True)
	state = models.CharField(max_length=255, blank=True, null=True)
	country = models.CharField(max_length=255, blank=True, null=True)
	operator = models.CharField(max_length=255, blank=True, null=True)
	oba = models.CharField(max_length=255, blank=True, null=True)
	gt = models.CharField(max_length=255, blank=True, null=True)
	miles = models.CharField(max_length=255, blank=True, null=True)
	qty_reason = models.CharField(max_length=255, blank=True, null=True)
	flw_cnt = models.CharField(max_length=255, blank=True, null=True)
	date = models.DateTimeField(auto_now_add=True)
	egm = models.CharField(max_length=255, blank=True, null=True)

	class Meta:
		db_table = 'pepl'
		verbose_name_plural = "PEPL"

class TIGT(models.Model):
	location_name = models.CharField(max_length=255, blank=True, null=True)
	location = models.CharField(max_length=255, blank=True, null=True)
	designcapacity = models.CharField(max_length=255, blank=True, null=True)
	operationallyavailablecapacity = models.CharField(max_length=255, blank=True, null=True)
	totalscheduledquantity = models.CharField(max_length=255, blank=True, null=True)
	operatingcapacity = models.CharField(max_length=255, blank=True, null=True)
	bi_di = models.CharField(max_length=255, blank=True, null=True)
	locsegment = models.CharField(max_length=255, blank=True, null=True)
	unsubscribedcapacity = models.CharField(max_length=255, blank=True, null=True)
	eff_gas_dat_time = models.DateTimeField(null=True, blank=True)
	post_date_time = models.DateTimeField(null=True, blank=True)
	it = models.CharField(max_length=255, blank=True, null=True)
	comments = models.CharField(max_length=255, blank=True, null=True)
	qty_reason = models.CharField(max_length=255, blank=True, null=True)
	all_qty_avail = models.CharField(max_length=255, blank=True, null=True)
	date = models.DateTimeField(auto_now_add=True)

	class Meta:
		db_table = 'tigt'
		verbose_name_plural = "TIGT"

class TPC(models.Model):
	location_name = models.CharField(max_length=255, blank=True, null=True)
	location = models.CharField(max_length=255, blank=True, null=True)
	designcapacity = models.CharField(max_length=255, blank=True, null=True)
	operationallyavailablecapacity = models.CharField(max_length=255, blank=True, null=True)
	totalscheduledquantity = models.CharField(max_length=255, blank=True, null=True)
	operatingcapacity = models.CharField(max_length=255, blank=True, null=True)
	bi_di = models.CharField(max_length=255, blank=True, null=True)
	locsegment = models.CharField(max_length=255, blank=True, null=True)
	unsubscribedcapacity = models.CharField(max_length=255, blank=True, null=True)
	eff_gas_dat_time = models.DateTimeField(null=True, blank=True)
	post_date_time = models.DateTimeField(null=True, blank=True)
	it = models.CharField(max_length=255, blank=True, null=True)
	comments = models.CharField(max_length=255, blank=True, null=True)
	qty_reason = models.CharField(max_length=255, blank=True, null=True)
	all_qty_avail = models.CharField(max_length=255, blank=True, null=True)
	date = models.DateTimeField(auto_now_add=True)

	class Meta:
		db_table = 'tpc'
		verbose_name_plural = "TPC"

class Alliance(models.Model):
	location_name = models.CharField(max_length=255, blank=True, null=True)
	post_date_time = models.DateTimeField(null=True, blank=True)
	eff_gas_dat_time = models.DateTimeField(null=True, blank=True)
	cycle = models.CharField(max_length=255, blank=True, null=True)
	loc_purp_desc = models.CharField(max_length=255, blank=True, null=True)
	location = models.CharField(max_length=255, blank=True, null=True)
	loc_qti = models.CharField(max_length=255, blank=True, null=True)
	all_qty_avail = models.CharField(max_length=255, blank=True, null=True)
	designcapacity = models.CharField(max_length=255, blank=True, null=True)
	operatingcapacity = models.CharField(max_length=255, blank=True, null=True)
	totalscheduledquantity = models.CharField(max_length=255, blank=True, null=True)
	operationallyavailablecapacity = models.CharField(max_length=255, blank=True, null=True)
	flow_ind = models.CharField(max_length=255, blank=True, null=True)
	it = models.CharField(max_length=255, blank=True, null=True)
	qty_reason = models.CharField(max_length=255, blank=True, null=True)
	date = models.DateTimeField(auto_now_add=True)

	class Meta:
		db_table = 'alliance'
		verbose_name_plural = "Alliance"