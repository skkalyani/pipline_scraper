import django_tables2 as tables
from .models import *

class ANRTable(tables.Table):
    class Meta:
        model = ANR
        template_name = 'django_tables2/bootstrap.html'

class PEPLTable(tables.Table):
    class Meta:
        model = PEPL
        template_name = 'django_tables2/bootstrap.html'

class TIGTTable(tables.Table):
    class Meta:
        model = TIGT
        template_name = 'django_tables2/bootstrap.html'

class TPCTable(tables.Table):
    class Meta:
        model = TPC
        template_name = 'django_tables2/bootstrap.html'

class AllianceTable(tables.Table):
    class Meta:
        model = Alliance
        template_name = 'django_tables2/bootstrap.html'
