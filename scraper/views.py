from django.shortcuts import render
from django_tables2 import RequestConfig
from django.http import HttpResponse
from django.views import generic
from .tables import *
from .models import *
import datetime

# Create your views here.
def home(request):
    table = ANRTable(ANR.objects.filter(eff_gas_dat_time__contains=datetime.datetime.now().date()))
    date = datetime.datetime.now().date().strftime("%m/%d/%Y")
    RequestConfig(request).configure(table)
    return render(request, 'index.html', {'table': table, 'pipline':"ANR", 'date':date})

def pipline(request):
    if request.method == "POST":
        pipline = request.POST.get("pipline", '')
        date = request.POST.get("date", '')
        model, modeltable = get_model_table(pipline)
        if not date:
            date = datetime.datetime.now().date().strftime("%m/%d/%Y")
        table = modeltable(model.objects.filter(eff_gas_dat_time__contains=datetime.datetime.strptime(date, '%m/%d/%Y').date()))
        RequestConfig(request, paginate=False).configure(table)
        return render(request, 'index.html', {'table': table, 'pipline':pipline, 'date':date})

def get_model_table(pipline):
    if pipline == "ANR":
        return (ANR, ANRTable)
    elif pipline == "PEPL":
        return (PEPL, PEPLTable)
    elif pipline == "TIGT":
        return (TIGT, TIGTTable)
    elif pipline == "TPC":
        return (TPC, TPCTable)
    elif pipline == "Alliance": 
        return (Alliance, AllianceTable)
    else:
        return ('','')